FROM nginxinc/nginx-unprivileged:1-alpine
LABEL maintainer="therafaelreis@gmail.com"

COPY ./default.conf.tpl /etc/nginx/default.conf.tpl
COPY ./uwsgi_params /etc/nginx/uwsgi_params

ENV LISTEN_PORT=8000
ENV APP_HOST=app
ENV APP_PORT=9000

USER root

RUN mkdir -p /vol/static
# Setting permissions
RUN chmod 755 /vol/static
# Create an empty file
RUN touch  /etc/nginx/conf.d/default.conf
# Change ownership to nginx
RUN chown nginx:nginx /etc/nginx/conf.d/default.conf

COPY ./entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

USER nginx

# default command
CMD ["/entrypoint.sh"]
